FROM registry.gitlab.com/topi.kettunen/swarm-slave:latest

RUN apt-get update && apt-get install -y \
    git \
    curl \
    python3 \
    python3-pip \
    python3-setuptools \
    docker-compose \
    nano \
    firefox \
    && rm -rf /var/lib/apt/lists/*

RUN pip3 install robotframework \
	robotframework-seleniumlibrary \
	robotframework-databaselibrary \
	robotremoteserver \
	mysql-connector-python-rf \
	requests \
	influxdb

COPY inspect.sh inspect.sh
RUN chmod ugo+x inspect.sh

COPY run-tests.sh run-tests.sh
RUN chmod ugo+x run-tests.sh

COPY query_for_fails.py query_for_fails.py
RUN chmod ugo+x query_for_fails.py

COPY tests tests

COPY run-docker-compose.sh run-docker-compose.sh
RUN chmod ugo+x run-docker-compose.sh

COPY docker-compose.yml docker-compose.yml

ENV DISPLAY=xvfb:0
ENV NAME=robot-slave
ENV LABELS="robot"

RUN wget https://github.com/mozilla/geckodriver/releases/download/v0.21.0/geckodriver-v0.21.0-linux64.tar.gz
RUN tar -xvzf geckodriver-v0.21.0-linux64.tar.gz
RUN rm geckodriver-v0.21.0-linux64.tar.gz
RUN chmod +x geckodriver
RUN cp geckodriver /usr/local/bin/

COPY grafana-influx grafana-influx
RUN chmod ugo+x grafana-influx/insert-cpu-core-amount.sh