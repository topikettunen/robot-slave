STACK_NAME="db"

TR_TARGET_PORT=3306
TR_CONTAINER_NAME="db"
TR_SERVICE_ID=$(docker stack services "$STACK_NAME" --filter "name=${STACK_NAME}_${TR_CONTAINER_NAME}" -q)
TR_PUBLISHED_PORT=$(docker inspect --format="{{ range .Endpoint.Ports }}{{if eq .TargetPort $TR_TARGET_PORT}}{{.PublishedPort}}{{end}}{{end}}" "$TR_SERVICE_ID")

echo "$TR_PUBLISHED_PORT"
