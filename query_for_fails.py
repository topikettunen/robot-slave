import requests
import json

def main():
    request_for_fails = requests.get('http://rest:5000/quarantine/tests')
    failed_tests_json = request_for_fails.json()
    with open('/tests/quarantine.txt', 'a') as f:
        for uuid in failed_tests_json['failed_test_uuids']:
            f.write('--exclude ' + uuid + '\n')

if __name__ == "__main__":
    main()
