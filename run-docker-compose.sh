#!/bin/bash

python3 /query_for_fails.py
sed -i 's/--exclude/--include/g' /tests/quarantine.txt

while [[ "$(curl -s -o /dev/null -w ''%{http_code}'' http://petclinic:8080/)" != "200" ]]; do
    sleep 5
done

rm -rf /quarantine-report/* | true
rm -rf /capture/* | true

/grafana-influx/insert-cpu-core-amount.sh &
docker-compose up -d xvfb i3wm ffmpeg
robot --argumentfile /tests/quarantine.txt --listener TestListener --listener InfluxListener --pythonpath /tests/lib --outputdir /quarantine-report /tests
docker cp ffmpeg:/capture /capture | true
docker-compose stop

rm /tests/quarantine.txt
