#!/bin/bash

python3 /query_for_fails.py

while [[ "$(curl -s -o /dev/null -w ''%{http_code}'' http://petclinic:8080/)" != "200" ]]; do
    sleep 5
done

rm -rf /report/* | true

/grafana-influx/insert-cpu-core-amount.sh &
robot --argumentfile /tests/quarantine.txt --listener TestListener --listener InfluxListener --pythonpath /tests/lib --outputdir /report /tests

rm /tests/quarantine.txt
