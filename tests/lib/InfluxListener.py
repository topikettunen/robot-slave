from robot.api import logger
from influxdb import InfluxDBClient
import datetime
import uuid

class InfluxListener():
  ROBOT_LISTENER_API_VERSION = 3

  # UUID of this testrun
  testrun_uuid = uuid.uuid1()

  # Collect all tags statistics from testcases here during run
  runtags = {}

  # Collect all tags statistics from testcases here during suite
  suitetags = {}

  def start_suite(self, suite, result):
    self.suitetags[suite.longname] = {}

  def end_test(self, test, result):

    current_suitename = test.parent.longname

    base_tags = {
      "testrun": self.testrun_uuid,
      "testcase": test.name,
      "testsuite": current_suitename
    }

    test_tags = {}
    for tag in result.tags:
      tag = "tctag_"+tag.lower()
      test_tags[tag]=1
      self._addTag(tag, self.suitetags[current_suitename])
      self._addTag(tag, self.runtags)

    test_tags.update(base_tags)

    json_body = [
      {
          "measurement": "testresult",
          "tags":  test_tags,
          "fields": {
              "test_count": 1,
              "status": result.status,
              "fail": 1 if (result.passed==False and result.critical==True) else 0,
              "noncritical_fail": 1 if (result.passed==False and result.critical==False) else 0,
              "pass": 1 if result.passed==True else 0,
              "elapsed_time": result.elapsedtime
          }
      }
    ]

    self._pushToInflux(json_body)


  def end_suite(self, suite, result):

    hard_tags = {
      "testrun": self.testrun_uuid,
      "suite_name": suite.name,
      "suite_longname": suite.longname
    }
    tags = suite.metadata.copy()
    tags.update(hard_tags)

    fields = {
      "status": result.status,
      "fail": 1 if result.passed==False else 0,
      "pass": 1 if result.passed==True else 0,
      "critical_cases_run": result.statistics.critical.total,
      "critical_cases_failed": result.statistics.critical.failed,
      "critical_cases_passed": result.statistics.critical.total-result.statistics.critical.failed,
      "total_cases_run": result.statistics.all.total,
      "total_cases_failed": result.statistics.all.failed,
      "total_cases_passed": result.statistics.all.total-result.statistics.all.failed,
      "elapsed_time": result.elapsedtime
    }
    fields.update(self.suitetags[suite.longname])

    json_body = [
      {
          "measurement": "suiteresult",
          "tags": tags,
          "fields": fields
      }
    ]

    self._pushToInflux(json_body)


  def close(self):

    tags = {
      "testrun_uuid": self.testrun_uuid
    }

    fields = {
      "runcount": 1
    }

    json_body = [
      {
          "measurement": "runresult",
          "tags": tags,
          "fields": fields
      }
    ]

    self._pushToInflux(json_body)


  def _addTag(self, tag, target):
      if tag in target.keys():
        target[tag] += 1
      else:
       target[tag] = 1

  def _pushToInflux(self, data):
    client = InfluxDBClient('influx', port=8086, username='robot', password='robot', database='robotframework')
    client.create_database("robotframework")
    client.write_points(data)
