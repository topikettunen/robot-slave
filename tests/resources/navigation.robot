*** Settings ***
Library    SeleniumLibrary

*** Variables ***
${TITLE}                   xpath=//h2

${FIND_OWNERS_PAGE_LINK}   xpath=//a[@href='/owners/find']

${SEARCH_OWNER_INPUT}      //*[@id="lastName"]
${SEARCH_OWNER_BUTTON}     //*[@id="search-owner-form"]/div[2]/div/button
${SEARCH_OWNER_NAME}       Rohamo
${CERTAIN_OWNER_URL}       ${SERVER}/owners/11

*** Keywords ***
User should see appropriate title
    Wait Until Element Is Visible    ${TITLE}
    Element Should Contain    ${TITLE}    ${PAGE_TITLE}

User navigates to vets page
    Wait Until Element Is Visible    ${VETS_PAGE_LINK}
    Click Element    ${VETS_PAGE_LINK}

User navigates to find owners page
    Wait Until Element Is Visible    ${FIND_OWNERS_PAGE_LINK}
    Click Link    ${FIND_OWNERS_PAGE_LINK}

User navigates to all owners page
    Wait Until Element Is Visible    ${ALL_OWNERS_PAGE_LINK}
    Click Element    ${ALL_OWNERS_PAGE_LINK}

User searches for certain owner
    Wait Until Element Is Visible    ${SEARCH_OWNER_INPUT}
    Input text    ${SEARCH_OWNER_INPUT}    ${SEARCH_OWNER_NAME}
    Click Element    ${SEARCH_OWNER_BUTTON}
    
User goes to certain owner's page
    Go To    ${CERTAIN_OWNER_URL}
