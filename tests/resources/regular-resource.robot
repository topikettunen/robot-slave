*** Settings ***
Library    SeleniumLibrary

*** Variables ***
${SERVER}      http://petclinic:8080
${GRID_URL}   http://hub:4444/wd/hub
${BROWSER}    Chrome
${DELAY}      0   

*** Keywords ***
Suite Setup
    Open Browser    ${SERVER}    ${BROWSER}    None    ${GRID_URL}    version:ANY
    Set Selenium Speed    ${DELAY}

Suite Teardown
    Close Browser

