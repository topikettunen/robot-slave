*** Settings ***
Suite Setup       Suite Setup
Suite Teardown    Suite Teardown
Resource          ../resources/navigation.robot
Resource          ../resources/resource.robot
Metadata          SUITE_UUID  c8db8d84-af41-11e8-98d0-529269fb1459
Test Template     Save three new pets

*** Test Cases ***
Add pet named Matti    Matti  2010-01-01  ${PET_CHOOSE_CAT}
  [Tags]  TEST_UUID_7c94c6c2-9bb7-11e8-98d0-529269fb1459
Add pet named Teppo    Teppo  2015-05-22  ${PET_CHOOSE_LIZARD}
  [Tags]  TEST_UUID_7c94c7e4-9bb7-11e8-98d0-529269fb1459
Add pet named Seppo    Seppo  2019-07-26  ${PET_CHOOSE_SNAKE}
  [Tags]  TEST_UUID_9939763c-9bcc-11e8-98d0-529269fb1459

*** Keywords ***
User navigates to add new pet
    User goes to certain owner's page
    Click Link  Add New Pet

Save three new pets
    User navigates to add new pet
    [Arguments]   ${petName}   ${birthDate}   ${petType}
    Input text    name  ${petName}
    Input text    birthDate   ${birthDate}
    Click Element  ${petType}
    Click Button  Add Pet

*** Variables ***
${PET_CHOOSE_CAT}        xpath=//option[@value='cat']
${PET_CHOOSE_LIZARD}     xpath=//option[@value='lizard']
${PET_CHOOSE_SNAKE}      xpath=//option[@value='snake']
${PET_CHOOSE_DOG}        xpath=//option[@value='dog']
${PET_CHOOSE_HAMSTER}    xpath=//option[@value='hamster']
