*** Settings ***
Suite Setup       Suite Setup
Suite Teardown    Suite Teardown
Resource          ../resources/navigation.robot
Resource          ../resources/resource.robot
Metadata          SUITE_UUID  c8db93b0-af41-11e8-98d0-529269fb1459

*** Variables ***
${PAGE_TITLE}   Welcome

*** Test Cases ***
User can see front page title
    User should see appropriate title
    [Tags]  TEST_UUID_c27e1516-9bcc-11e8-98d0-529269fb1459
